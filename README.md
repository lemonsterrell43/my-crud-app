# CREATE • READ • UPDATE • DELETE (CRUD) APP

## App that creates CRUD functionality

Express, Node, and MongoDB is the best way to learn CRUD functionality.

## Express
Express is a framework for building web applications on top of Node.js. It simplifies the server creation process that is already available in Node. Node allows you to use JavaScript as your server-side language.

## MongoDB
MongoDB is a database. This is the place where you store information for your websites (or applications).

## CRUD
CRUD is an acronym for Create, Read, Update and Delete. This is what each operation does:

- Create ----- (POST) -> Make something
- Read -------- (GET) -> Get something
- Update ------ (PUT) -> Change something•
- Delete --- (DELETE) -> Remove something

`POST`, `GET`, `PUT`, and `DELETE` requests let us construct Rest APIs.

## Getting started

Let’s build a simple application that lets you track a list of Exotic Cars Collection. 

Start by creating a folder for this project. After you’ve created the folder, navigate into it with the Terminal and run:

```bash
    $ npm init
```

`npm init` creates a `package.json` file which helps you manage dependencies (which we will install as we go through the lesson). Just hit enter through everything that appears. Or you can auto populate the package.json with:

```bash
    $ npm init -y
``` 

## Running Node

The simplest way to use node is to run the `node` command, and specify a path to a file. Let’s create a file called `server.js` to run node with.

```bash
    $ touch server.js
``` 

Next, put this `console.log` statement into `server.js`. This lets us know whether Node is running properly.

```bash
    // server.js
    $ console.log('Node is Up and Running')
``` 
Now, run `node server.js` in your command line and you should see this:

```bash
    ~/Desktop/crud-app$ node server.js
    > Node is Up and Running
``` 
If you see the above, Great! Node works. The next step is to understand how to use Express.

## Using Express

First, we have to install Express. We can do this by running the `npm install` command. (`npm` is installed with Node, which is why you use commands like `npm init` and `npm install`).

Run `npm install express --save` command in your command line.[^1]


[^1]: The `--save` flag saves `express` as a `dependency` in `package.json`. It’s important to know these dependencies because `npm` can retrieve dependencies with another `npm install` command when you need it later.

```bash
    $ npm install express --save
``` 

The results:
```bash
    // package.json
    "dependencies": {
        "express": "^4.17.1"
    }
``` 
Next, we use express in `server.js` by requiring it.

```bash
    const express = require('express');
    const app = express();
``` 
We need to create a server that browsers can connect to. We do this by using the Express’s `listen` method.

```bash
    app.listen(5050, function() {
        console.log('listening on 5050')
    })
``` 
Now, run `node server.js` and navigate to `localhost:5050` on your browser. You should see a message that says `Cannot GET/`.

That’s a good sign. It means __we can now communicate to our express server through the browser__. This is where we begin CRUD operations.

## CRUD - READ

Browsers perform the **READ** operation when you visit a website. Under the hood, they send a **GET** request to the server to perform this READ operation.

You see cannot `get/` because our server sent nothing back to the browser.

In Express, we handle a **GET** request with the `get` method:

```bash
    $ app.get(endpoint, callback)
``` 

`endpoint` **is the requested endpoint**. It’s the value that comes after your domain name. Here is an examples:

When you visit `localhost:5050`, you’re actually visiting `localhost:3000/`. In this case, browsers requested for `/`.

`callback` tells the server what to do when the requested endpoint matches the endpoint stated. It takes two arguments: A `request` object and a `response` object.

```bash
    // We normally abbreviate `request` to `req` and `response` to `res`.
    app.get('/', function (req, res) {
        // do something here
    })
``` 

For now, let’s write Hello World back to the browser. We do so by using a `send` method that comes with the `response` object:

```bash
    app.get('/', function(req, res) {
        res.send('Hello World')
    })
``` 

I’m going to start writing in ES6 code and show you how to convert to ES6 along the way as well. First off, I’m replacing `function()` with an ES6 arrow function. The below code is the same as the above code:

```bash
    app.get('/', (req, res) => {
        res.send('Hello World')
    })
```

Now, restart your server by doing the following:

 1. Stop the current server by hitting `CTRL + C` in the command line.

 2. Run `node server.js` again.

Then, navigate to `localhost:5050` on your browser. You should be able to see a string that says “Hello World”.

Great.

Next, let’s change `server.js` so we serve up an `index.html` page back to the browser. To do this, we use the `sendFile` method that’s provided by the `res` object.

```bash
    app.get('/', (req, res) => {
        res.sendFile(__dirname + '/index.html')
        // Note: __dirname is the current directory you are in. Try logging it and see what you get!
    })
```

In the sendFile method above, we told Express to serve an index.html file that can be found in the root of your project folder. We don’t have that file yet. Let’s make it now.

```bash
    touch index.html
```

Let’s put some text in our `index.html` file as well:

```bash
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>CRUD APP</title>
        </head>
        <body>
            <h1> My First CRUD functionality. </h1>
        </body>
    </html>
```

Restart your server and refresh your browser. You should be able to see your HTML file now.

This is how Express handles a **GET** request (**READ** operation).

Restart your server whenever you make a change to `server.js`. This process is repetitive, so let’s detour and use a tool called nodemon.

## Enter Nodemon

**Nodemon restarts the server automatically** when you save a file that’s used by the server.js. We can install Nodemon with the following command:

```bash
    $ npm install nodemon --save-dev -g
```

We use a `--save-dev` flag here because we only use Nodemon when we are development. We won’t use Nodemon on an actual server. `--save-dev` here adds Nodeman as a `devDependency` in the `package.json` file.

```bash
    "devDependencies": {
        "nodemon": "^9.0.0"
    }
```

Nodemon behaves like Node. So you can run `nodemon server.js` and you’d expect to see the same thing. Unfortunately, this only works if you’ve installed nodemon globally with the `-g` flag.

We can make things simpler by adding  script key in the package.json file. This lets us run nodemon server.js without the ./node_modules... preamble.

```bash
        {
        // ...
        "scripts": {
            "dev": "nodemon server.js"
        }
        // ...
    }
```

Now, you can run `npm run dev` to trigger nodemon server.js.

# Now we’re going to cover the CREATE operation next.

## CRUD - CREATE
Browsers can only perform a **CREATE** operation if they send **POST** request to the server. This `POST` request can be triggered through JavaScript or through a `<form>` element.

Let’s figure out how to use a `<form>` element to create new entries for Exotic Car Collection application for now. We’ll examine how to send requests via JavaScript later.

To send a `POST` request through a `<form>`, you need to add the `<form>` element to your index.html file.

You need three things on this form element:

 1. An action attribute
 2. A method attribute
 3. `name` attributes on each `<input>` elements within the form

```bash
    <form action="/cars" method="POST">
        <input type="text" placeholder="car" name="car">
        <input type="text" placeholder="model" name="model">
        <button type="submit">Submit</button>
    </form>
```

The `method` tells browsers what kind of request to send. In this case, we use `POST` because we’re sending a `POST` request.

The `action` attribute tells the browser where to send the `POST` request. In this case, we’re send the `POST` request to `/cars`.

We can handle this `POST` request with a `post` method in `server.js`. The `path` should be the value you placed in the `action` attribute.

```bash
    app.post('/cars', (req, res) => {
        console.log('VROOOM')
    })
```

Restart your server and refresh your browser. 

Then, enter something into the `<form>` element and submit the form. 

Next, look at your command line. You should see `VROOOM` in your command line.

Great, we know that Express is handling the form for us right now. The next question is, how do we get the input values with Express?

Turns out, Express doesn’t handle reading data from the `<form>` element on it’s own. We have to add another package called `body-parser` to gain this functionality.

```bash
    $ npm install body-parser --save
```

Body-parser is a *middleware*. They help to tidy up the `request` object before we use them. Express lets us use middleware with the `use` method.

```bash
    const express = require('express')
    const bodyParser= require('body-parser')
    const app = express()


    // Make sure you place body-parser before your CRUD handlers!
    app.use(bodyParser.urlencoded({ extended: true }))

    // All your handlers here...
    app.get('/', (req, res) => { /*...*/ })
    app.post('/cars', (req, res) => { /*...*/ })
```

The `urlencoded` method within body-parser tells it to extract data from the `<form>` element and add them to the `body` property in the `request` object.

You should be able to see values from the `<form>` element inside `req.body` now. Try doing a `console.log` and see what it is!

```bash
    app.post('/cars', (req, res) => {
        console.log(req.body)
    })
```

You should see an object similar to the following:

```bash
    {
        name: 'Buick',
        model: 'Lacrosse'
    }
```

Enter the database, MongoDB.

## MongoDB

MongoDB is a database. We can store information into this database to remember our Exotic Car Collection. Then, we can retrieve this information and display to people who view our app.

First, we need to install MongoDB via npm.

```bash
    $ npm install mongodb --save
```

Once installed, we can connect to MongoDB through the MongoClient’s connect method as shown in the code below:

```bash
    const MongoClient = require('mongodb').MongoClient
```

You can connect to your local MongoDB with this url:

```bash
    const url = 'mongodb://127.0.0.1:27017'
```

With the Mongo Client, you need to specify the database you’re using after you connect to MongoDB. Here’s what it looks like:

```bash
    const db_name = 'exotic-car-collection'
    let db

    MongoClient.connect(url, { 

        useNewUrlParser: true 
    
    }, (err, client) => {
        
        if (err) return console.log(err)

    // Storing a reference to the database so you can use it later
    db = client.db(db_name)
    console.log(`Connected MongoDB: ${url}`)
    console.log(`Database: ${db_name}`)
    })
```

Connected to local MongoDB with native MongoDB driver.

```bash
    [test] node server.js
    Connected NongoDB: mongodb://127.0.0.1:27017
    Database: exotic-car-collection
```

### Connecting with Mongoose

To connect with Mongoose, you need to download and require `mongoose`.

```bash
    npm install mongoose --save
```

```bash
    const mongoose = require('mongoose')
```

When you use Mongoose, the connection `url` should include the database you’re connecting to:

```bash
    const url = 'mongodb://127.0.0.1:27017/exotic-car-collection'
```

You can connect to MongoDB with the `connect` method:

```bash
    mongoose.connect(url, { useNewUrlParser: true })
```

Here’s how you can check whether the connection succeeds.

```bash
    const db = mongoose.connection
    db.once('open', _ => {
        console.log('Database connected:', url)
    })

    db.on('error', err => {
        console.error('connection error:', err)
    })
```
RESULTS:

```bash
    [test] node server.js
    Database connected: mongodb://127.0.0.1:27017/exotic-car-collection
```








## CRUD - CREATE (continued)

We need to create a collection before we can store items into a database. Here’s a simple analogy to help you clear up the terms in MongoDB:

Imagine a Database is a Room.
A Room contains boxes (collections).
Like Databases, you can name collections anything you want. In this case, let’s store cars into a `car's` collection. We use `db.collection` to specify the collection.

```bash
    MongoClient.connect( /* ... */ )
    .then(client => {
        // ...
        const db = client.db('exotic-car-collection')
        const carsCollection = db.collection('cars')

        // ...
    })
```

We can use the `insertOne` method to add items into a MongoDB collection.

```bash
    app.post('/cars', (req, res) => {
    carsCollection.insertOne(req.body)
        .then(result => {
        console.log(result)
        })
        .catch(error => console.error(error))
    })
```

Try submitting the `<form>` from the browser. You should see data in the Terminal.

If you see data from cars, congratulations! You’ve successfully add the car into the database.

However, your browser still trying to load a page. This happens because the browser expects something back from the server.

In this case, we don’t need to send the browser information. Let’s ask the browser to redirect back to `/` instead. We do this with `res.redirect`.

```bash
    app.post('/cars', (req, res) => {
    carsCollection.insertOne(req.body)
        .then(result => {
        res.redirect('/')
        })
        .catch(error => console.error(error))
    })
```

Redirected. Browser is no longer waiting to load something.

Since we have some cars in the collection, let’s show them to our user when they land on the page!


```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 

```bash
    $ npm init -y
``` 


